package com.bongat.s01activity;

public class S01Activity {
    public static void main(String[] args) {
        int age = 24;
        double height = 168.85;
        char gender = 'M';
        boolean isJavaCool = true;

        System.out.println("My name is Jordan, I am " + age + "yrs old, \nMy gender is " + gender + " short for Male, \nMy height is " + height + "cm, \nI think it is " + isJavaCool + " that Java is Amazing");
    }
}
